import useBooksContext from "../hooks/use-books-context";
import { useState } from "react";

export default function BookEdit ({ book, onSubmit }) {
    const { editBookById } = useBooksContext();
    const [title, setTitle] = useState(book.title);

    const handleChange = (event) => {
        setTitle(event.target.value);
    };

    const handleSubmit = (event) => {
        event.preventDefault();
        editBookById(book.id, title);
        onSubmit();
    };

    return (
        <form onSubmit={handleSubmit} className="book-edit" >
            <label>Title</label>
            <input className="input" value={title} onChange={handleChange} />
            <button className="button is-primary">
                Save
            </button> 
        </form>
    );
}